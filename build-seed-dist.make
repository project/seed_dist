api = 2
core = 7.41
projects[drupal][type] = core

projects[seed_dist][type] = profile
projects[seed_dist][version] = 1.x-dev
projects[seed_dist][download][type] = git
projects[seed_dist][download][url] = http://git.drupal.org/project/seed_dist.git
projects[seed_dist][download][branch] = 7.x-1.x
