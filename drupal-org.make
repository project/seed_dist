api = 2
core = 7.41

; Drupal Projects

projects[context][version] = 3.6
projects[context][subdir] = contrib

projects[ctools][version] = 1.9
projects[ctools][subdir] = contrib

projects[devel][version] = 1.5
projects[devel][subdir] = contrib

projects[entity][version] = 1.6
projects[entity][subdir] = contrib

projects[google_analytics] = 2.1
projects[google_analytics][subdir] = contrib

projects[i18n][version] = 1.13
projects[i18n][subdir] = contrib

projects[l10n_update][type] = module
projects[l10n_update][download][type] = git
projects[l10n_update][download][revision] = 2218fa4
projects[l10n_update][download][branch] = 7.x-1.x
projects[l10n_update][subdir] = contrib

projects[libraries][version] = 2.2
projects[libraries][subdir] = contrib

projects[mail_headers][version] = 1.0
projects[mail_headers][subdir] = contrib

projects[masquerade][version] = 1.0-rc7
projects[masquerade][subdir] = contrib

projects[metatag][version] = 1.7
projects[metatag][subdir] = contrib

projects[module_filter][version] = 2.0
projects[module_filter][subdir] = contrib

projects[pathauto][version] = 1.3
projects[pathauto][subdir] = contrib

projects[pathologic][version] = 2.12
projects[pathologic][subdir] = contrib

projects[redirect][version] = 1.0-rc3
projects[redirect][subdir] = contrib

projects[token][version] = 1.6
projects[token][subdir] = contrib

projects[transliteration][version] = 3.2
projects[transliteration][subdir] = contrib

projects[variable][version] = 2.5
projects[variable][subdir] = contrib

projects[views][version] = 3.13
projects[views][subdir] = contrib

projects[wysiwyg][version] = 2.2
projects[wysiwyg][subdir] = contrib

; Modified projects from D.O.

projects[globalredirect][version] = 1.5
projects[globalredirect][patch][] = http://drupal.org/files/globalredirect-correct_use_of_prefixes-1843500-3.patch
projects[globalredirect][patch][] = http://drupal.org/files/globalredirect-base%20globalredirect_request_path_in_%20request_path-1843500-3.patch
projects[globalredirect][patch][] = http://drupal.org/files/globalredirect-correct_use_of_prefixes-1843500-4.patch
projects[globalredirect][subdir] = mods


; SeeD Family
projects[seed_tools][version] = 1.0-alpha1
projects[seed_tools][subdir] = contrib

projects[seed][version] = 2.0-beta6

; libraries
; NOTE: These need to be listed in http://drupal.org/packaging-whitelist.
libraries[tinymce][download][type] = "wget"
libraries[tinymce][type] = "libraries"
libraries[tinymce][download][url] = "http://download.moxiecode.com/tinymce/tinymce_3.5.8.zip"
